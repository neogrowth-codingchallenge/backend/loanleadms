using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using LoanLeadMs.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LoanLeadMs.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CommunicationsController : ControllerBase
    {
        private readonly ICommunicationService _service;
        private readonly ILogger<CommunicationsController> _logger;

        public CommunicationsController(ICommunicationService service, ILogger<CommunicationsController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet("{leadId}")]
        [ProducesResponseType(typeof(IEnumerable<CommunicationLog>), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int leadId)
        {
            return Ok(await _service.GetCommunicationLogByLeadId(leadId));
        }

        [HttpPost]
        [ProducesResponseType(typeof(CommunicationLog), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post(CommunicationLog log)
        {
            return Ok(await _service.Add(log));
        }

    }
}