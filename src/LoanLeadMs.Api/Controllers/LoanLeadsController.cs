﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using LoanLeadMs.Api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LoanLeadMs.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoanLeadsController : ControllerBase
    {
        private readonly ILoanLeadService _service;
        private readonly ILogger<LoanLeadsController> _logger;

        public LoanLeadsController(ILoanLeadService service, ILogger<LoanLeadsController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<LoanLead>),200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.GetLeads());
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(LoanLead),200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await _service.GetLead(id));
        }

        [HttpPost]
        [ProducesResponseType(typeof(LeadDetail), 200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post(LeadDetail lead)
        {
            return Ok(await _service.AddLead(lead));
        }
    }
}
