using System;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using Microsoft.AspNetCore.Mvc;

namespace LoanLeadMs.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TypesController : ControllerBase
    {
        [HttpGet("{type}")]
        [ProducesResponseType(typeof(string[]), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(string type)
        {
            switch (type.ToLower())
            {
                case "communicationmode":
                    return Ok(Enum.GetNames(typeof(CommunicationMode)));
                case "contacttype":
                    return Ok(Enum.GetNames(typeof(ContactType)));
                case "gender":
                    return Ok(Enum.GetNames(typeof(Gender)));
                case "leadsource":
                    return Ok(Enum.GetNames(typeof(LeadSource)));
                case "leadstatus":
                    return Ok(Enum.GetNames(typeof(LeadStatus)));
                default:
                    break;

            }
            return NotFound();
        }
    }
}