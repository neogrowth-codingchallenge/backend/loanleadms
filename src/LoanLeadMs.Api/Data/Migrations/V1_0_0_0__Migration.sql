CREATE TABLE LoanLead(
    Id SERIAL PRIMARY KEY,
    ContactId INT NOT NULL,
    LoanAmount DECIMAL NOT NULL,
    Source SMALLINT NOT NULL,
    Status SMALLINT NOT NULL,
	CommunicationMode SMALLINT NOT NULL
);

CREATE TABLE ContactInfo(
    Id SERIAL PRIMARY KEY,
    OrganizationId INT NOT NULL, 
    ContactType SMALLINT NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Dob DATE,
    Gender SMALLINT NOT NULL,
    EmailId VARCHAR(50) NOT NULL,
    PhoneNumber VARCHAR(20) NOT NULL
);

CREATE TABLE OrganizationInfo(
    Id SERIAL PRIMARY KEY,
    Name VARCHAR(50) NOT NULL,
    Website VARCHAR(50),
    NatureOfBusiness VARCHAR(50)
);

CREATE TABLE CommunicationLog(
    Id SERIAL PRIMARY KEY,
    LeadId INT NOT NULL,
    LeadStatus SMALLINT NOT NULL,
	CommunicationMode SMALLINT NOT NULL,
    CommunicationDate Date NOT NULL
);