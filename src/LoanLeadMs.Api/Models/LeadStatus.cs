using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LoanLeadMs.Api.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LeadStatus
    {
        [Display(Name = "Initial Communication")]
        InitialCommunication,
        FollowUp,
        NotInterested,
        ConvertedToCustomer
    }
}