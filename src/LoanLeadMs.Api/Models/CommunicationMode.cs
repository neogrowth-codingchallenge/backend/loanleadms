using System.Text.Json.Serialization;

namespace LoanLeadMs.Api.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum CommunicationMode
    {
        Call,
        PersonalMeet
    }
}