using System.Text.Json.Serialization;

namespace LoanLeadMs.Api.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}