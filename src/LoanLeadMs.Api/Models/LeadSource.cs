using System.Text.Json.Serialization;

namespace LoanLeadMs.Api.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LeadSource
    {
        Emailer,
        DirectSalesAgent,
        NewsPaper,
        Marketing
    }
}