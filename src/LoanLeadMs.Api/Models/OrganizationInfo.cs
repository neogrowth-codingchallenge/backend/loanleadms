using System.ComponentModel.DataAnnotations;

namespace LoanLeadMs.Api.Models
{
    public class OrganizationInfo
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        public string Website { get; set; }

        public string NatureOfBusiness { get; set; }
    }
}