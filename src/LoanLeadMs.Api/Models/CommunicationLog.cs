using System;
using System.ComponentModel.DataAnnotations;

namespace LoanLeadMs.Api.Models
{
    public class CommunicationLog
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Lead id is required.")]
        public int LeadId { get; set; }

        [Required(ErrorMessage ="Mode is required.")]
        public CommunicationMode CommunicationMode { get; set; }

        [Required(ErrorMessage="Status is required.")]
        public LeadStatus LeadStatus { get; set; }

        public DateTime CommunicationDate { get; set; }
    }
}