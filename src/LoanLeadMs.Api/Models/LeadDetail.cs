namespace LoanLeadMs.Api.Models
{
    public class LeadDetail
    {
        public OrganizationInfo OrganizationInfo { get; set; }
        public ContactInfo ContactInfo { get; set; }
        public LoanLead LeadInfo { get; set; }
    }
}