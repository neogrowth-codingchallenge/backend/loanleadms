using System.Text.Json.Serialization;

namespace LoanLeadMs.Api.Models
{
     [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum ContactType
    {
        Type1,
        Type2
    }
}