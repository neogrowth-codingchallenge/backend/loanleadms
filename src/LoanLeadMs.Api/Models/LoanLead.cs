using System.ComponentModel.DataAnnotations;

namespace LoanLeadMs.Api.Models
{
    public class LoanLead
    {
        public int Id { get; set; }

        public int ContactId { get; set; }

        [Required(ErrorMessage="Loan amount is required")]
        public double LoanAmount { get; set; }

        public LeadSource Source { get; set; }

        public LeadStatus Status { get; set; }

        public CommunicationMode CommunicationMode { get; set; }
    }
}