using System;
using System.ComponentModel.DataAnnotations;

namespace LoanLeadMs.Api.Models
{
    public class ContactInfo
    {
        public int Id { get; set; }

        public int OrganizationId { get; set; }
        public ContactType ContactType { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        public DateTime Dob { get; set; }

        public Gender Gender { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string EmailId { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "Phone number is required")]
        [Phone(ErrorMessage = "Invalid Phone number")]
        [MinLength(10, ErrorMessage = "Length should be 10."), MaxLength(10, ErrorMessage = "Length should be 10.")]
        public string PhoneNumber { get; set; }
    }
}