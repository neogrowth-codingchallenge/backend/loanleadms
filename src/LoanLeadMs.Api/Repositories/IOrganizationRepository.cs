using System.Threading.Tasks;
using LoanLeadMs.Api.Models;

namespace LoanLeadMs.Api.Repositories
{
    public interface IOrganizationRepository
    {
         public Task<OrganizationInfo> AddOrganization(OrganizationInfo organizationInfo);
         public Task<OrganizationInfo> GetOrganization(int id);
    }
}