using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;

namespace LoanLeadMs.Api.Repositories
{
    public interface ICommunicationRepository
    {
         public Task<CommunicationLog> Add(CommunicationLog communicationLog);
         public Task<CommunicationLog> GetCommunicationLog(int id);

         public Task<IEnumerable<CommunicationLog>> GetCommunicationLogByLeadId(int leadId);
    }
}