using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LoanLeadMs.Api.Models;
using Npgsql;

namespace LoanLeadMs.Api.Repositories
{
    public class ContactInfoRepository : IContactInfoRepository
    {
        private readonly string _connectionString;

        public ContactInfoRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<ContactInfo> AddContactInfo(ContactInfo contactInfo)
        {
            var sQuery = "INSERT INTO ContactInfo(OrganizationId, ContactType, Name, Dob, Gender, EmailId, PhoneNumber)" +
            "VALUES(@OrganizationId, @ContactType, @Name, @Dob, @Gender, @EmailId, @PhoneNumber) RETURNING Id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                var result = await connection.QueryAsync<int>(sQuery, contactInfo);

                contactInfo.Id = result.FirstOrDefault();
                return contactInfo;
            }           
        }

        public async Task<ContactInfo> GetContactInfo(int id)
        {
            var sQuery = "SELECT Id, OrganizationId, ContactType, Name, Dob, Gender, EmailId, PhoneNumber FROM ContactInfo WHERE Id = @id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<ContactInfo>(sQuery, new
                {
                    id = id
                });

                return result.FirstOrDefault();
            }
        }
    }
}