using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LoanLeadMs.Api.Models;
using Npgsql;

namespace LoanLeadMs.Api.Repositories
{
    public class CommunicationRepository : ICommunicationRepository
    {
        private readonly string _connectionString;

        public CommunicationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<CommunicationLog> Add(CommunicationLog communicationLog)
        {
            var sQuery = "INSERT INTO CommunicationLog(LeadId, LeadStatus, CommunicationMode, CommunicationDate)" +
            "VALUES(@LeadId, @LeadStatus, @CommunicationMode, @CommunicationDate) RETURNING Id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                var result = await connection.QueryAsync<int>(sQuery, communicationLog);

                communicationLog.Id = result.FirstOrDefault();
                return communicationLog;
            }
        }

        public async Task<CommunicationLog> GetCommunicationLog(int id)
        {
            var sQuery = "SELECT Id, LeadId, LeadStatus, CommunicationMode, CommunicationDate FROM CommunicationLog WHERE Id = @id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<CommunicationLog>(sQuery, new
                {
                    id = id
                });

                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<CommunicationLog>> GetCommunicationLogByLeadId(int leadId)
        {
            var sQuery = "SELECT Id, LeadId, LeadStatus, CommunicationMode, CommunicationDate FROM CommunicationLog WHERE LeadId = @leadId";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<CommunicationLog>(sQuery, new
                {
                    leadId = leadId
                });

                return result;
            }
        }
    }
}