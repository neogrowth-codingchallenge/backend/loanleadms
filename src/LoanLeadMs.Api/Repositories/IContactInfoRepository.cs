using System.Threading.Tasks;
using LoanLeadMs.Api.Models;

namespace LoanLeadMs.Api.Repositories
{
    public interface IContactInfoRepository
    {
         public Task<ContactInfo> GetContactInfo(int id);
         public Task<ContactInfo> AddContactInfo(ContactInfo contactInfo);
    }
}