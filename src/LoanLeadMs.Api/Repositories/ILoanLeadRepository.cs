using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;

namespace LoanLeadMs.Api.Repositories
{
    public interface ILoanLeadRepository
    {
        public Task<LoanLead> Add(LoanLead loanLead);
        public Task<LoanLead> Get(int id);
        public Task<IEnumerable<LoanLead>> GetAll();

        public Task<bool> UpdateStatus(int id, LeadStatus status);
    }
}