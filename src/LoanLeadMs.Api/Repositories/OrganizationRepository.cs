using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LoanLeadMs.Api.Models;
using Npgsql;

namespace LoanLeadMs.Api.Repositories
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly string _connectionString;

        public OrganizationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<OrganizationInfo> AddOrganization(OrganizationInfo organizationInfo)
        {
            var sQuery = "INSERT INTO OrganizationInfo(Name, WebSite, NatureOfBusiness)" +
             "VALUES(@Name, @WebSite, @NatureOfBusiness) RETURNING Id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                var result = await connection.QueryAsync<int>(sQuery, organizationInfo);
                organizationInfo.Id = result.FirstOrDefault();
                return organizationInfo;
            }
        }

        public async Task<OrganizationInfo> GetOrganization(int id)
        {
            var sQuery = "SELECT Id, Name, WebSite, NatureOfBusiness FROM OrganizationInfo  WHERE Id = @id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<OrganizationInfo>(sQuery, new
                {
                    id = id
                });

                return result.FirstOrDefault();
            }
        }
    }
}