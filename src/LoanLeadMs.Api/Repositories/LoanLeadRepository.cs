using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using LoanLeadMs.Api.Models;
using Npgsql;

namespace LoanLeadMs.Api.Repositories
{
    public class LoanLeadRepository : ILoanLeadRepository
    {
        private readonly string _connectionString;

        public LoanLeadRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task<LoanLead> Add(LoanLead loanLead)
        {
            var sQuery = "INSERT INTO LoanLead(ContactId, LoanAmount, Source, Status, CommunicationMode)" +
            "VALUES(@ContactId, @LoanAmount, @Source, @Status, @CommunicationMode) RETURNING Id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();
                var result = await connection.QueryAsync<int>(sQuery, loanLead);

                loanLead.Id = result.FirstOrDefault();
                return loanLead;
            }
        }

        public async Task<LoanLead> Get(int id)
        {
            var sQuery = "SELECT Id,ContactId, LoanAmount, Source, Status, CommunicationMode FROM LoanLead  WHERE Id = @id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<LoanLead>(sQuery, new
                {
                    id = id
                });

                return result.FirstOrDefault();
            }
        }

        public async Task<IEnumerable<LoanLead>> GetAll()
        {
            var sQuery = "SELECT Id,ContactId, LoanAmount, Source, Status, CommunicationMode FROM LoanLead";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.QueryAsync<LoanLead>(sQuery);

                return result;
            }
        }

        public async Task<bool> UpdateStatus(int id, LeadStatus status)
        {
            var sQuery = "UPDATE LoanLead SET Status = @status WHERE Id = @id";
            using (IDbConnection connection = new NpgsqlConnection(_connectionString))
            {
                connection.Open();

                var result = await connection.ExecuteAsync(sQuery, new
                {
                    id = id,
                    status = status
                });

                return result > 0;
            }
        }
    }
}