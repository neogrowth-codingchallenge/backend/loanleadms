using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using LoanLeadMs.Api.Repositories;

namespace LoanLeadMs.Api.Services
{
    public class CommunicationService : ICommunicationService
    {
        private readonly ICommunicationRepository _communicationRepository;
        private readonly ILoanLeadRepository _leadRepository;

        public CommunicationService(ICommunicationRepository communicationRepository, ILoanLeadRepository leadRepository)
        {
            _communicationRepository = communicationRepository;
            _leadRepository = leadRepository;
        }
        public async Task<CommunicationLog> Add(CommunicationLog communicationLog)
        {
            // Check if Lead exists
            var lead = await _leadRepository.Get(communicationLog.LeadId);
            if(lead == null)
            {
                return null;
            }
            communicationLog.CommunicationDate = DateTime.Today;
            var log = await _communicationRepository.Add(communicationLog);
            await _leadRepository.UpdateStatus(communicationLog.LeadId, communicationLog.LeadStatus);

            return log;

        }

        public async Task<CommunicationLog> GetCommunicationLog(int id)
        {
            return await _communicationRepository.GetCommunicationLog(id);
        }

        public async Task<IEnumerable<CommunicationLog>> GetCommunicationLogByLeadId(int leadId)
        {
            return await _communicationRepository.GetCommunicationLogByLeadId(leadId);
        }
    }
}