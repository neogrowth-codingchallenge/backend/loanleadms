using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;

namespace LoanLeadMs.Api.Services
{
    public interface ILoanLeadService
    {
         public Task<LoanLead> GetLead(int id);

         public Task<IEnumerable<LoanLead>> GetLeads();

         public Task<LeadDetail> AddLead(LeadDetail lead);

    }
}