using System.Collections.Generic;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using LoanLeadMs.Api.Repositories;

namespace LoanLeadMs.Api.Services
{
    public class LoanLeadService : ILoanLeadService
    {
        private readonly IOrganizationRepository _organizationRepository;
        private readonly ILoanLeadRepository _leadRepository;
        private readonly IContactInfoRepository _contactInfoRepository;

        public LoanLeadService(
            IOrganizationRepository organizationRepository, 
            ILoanLeadRepository leadRepository, IContactInfoRepository contactInfoRepository)
        {
            _organizationRepository = organizationRepository;
            _leadRepository = leadRepository;
            _contactInfoRepository = contactInfoRepository;
        }
        public async Task<LeadDetail> AddLead(LeadDetail lead)
        {
            var orgInfo = await _organizationRepository.AddOrganization(lead.OrganizationInfo);
            lead.OrganizationInfo = orgInfo;                    
            lead.ContactInfo.OrganizationId = orgInfo.Id;
            var contact = await _contactInfoRepository.AddContactInfo(lead.ContactInfo);
            lead.ContactInfo = contact;
            lead.LeadInfo.ContactId = contact.Id;
            var leadInfo = await _leadRepository.Add(lead.LeadInfo);
            lead.LeadInfo.Id = leadInfo.Id;

            return lead;
        }

        public async Task<LoanLead> GetLead(int id)
        {
            return await _leadRepository.Get(id);
        }

        public async Task<IEnumerable<LoanLead>> GetLeads()
        {
            return await _leadRepository.GetAll();
        }
    }
}