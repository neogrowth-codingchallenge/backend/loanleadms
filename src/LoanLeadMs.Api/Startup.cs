using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using LoanLeadMs.Api.Repositories;
using LoanLeadMs.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace LoanLeadMs.Api
{
    public class Startup
    {
        private ILogger<Startup> _logger;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConnectionString = configuration.GetConnectionString("DbConnectionString");
        }

        public IConfiguration Configuration { get; }
        public string ConnectionString { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerDocument(options =>
            {
                options.DocumentName = "v1";
                options.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Loan Lead API";
                    document.Info.Description = "Loan Lead API v1";
                };
            });
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                options.JsonSerializerOptions.IgnoreNullValues = true;
            });
            services.AddScoped<ICommunicationService, CommunicationService>();
            services.AddScoped<ILoanLeadService, LoanLeadService>();
            services.AddScoped<ICommunicationRepository>(r => new CommunicationRepository(ConnectionString));
            services.AddScoped<ILoanLeadRepository>(r => new LoanLeadRepository(ConnectionString));
            services.AddScoped<IContactInfoRepository>(r => new ContactInfoRepository(ConnectionString));
            services.AddScoped<IOrganizationRepository>(r => new OrganizationRepository(ConnectionString));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            InitialiseDatabase();
            app.UseHttpsRedirection();

            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseCors(c => c.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void InitialiseDatabase()
        {
            try
            {
                _logger.LogInformation("Initialising Database..");
                _logger.LogInformation(ConnectionString);
                var connection = new NpgsqlConnection(ConnectionString);
                var evolve = new Evolve.Evolve(connection)
                {
                    Locations = new[] { "Data/Migrations" },
                    IsEraseDisabled = true
                };

                evolve.Migrate();
                _logger.LogInformation("Database Initialisation complete..");
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}