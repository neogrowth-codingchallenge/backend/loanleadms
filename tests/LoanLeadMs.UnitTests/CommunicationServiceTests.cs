using System;
using System.Threading.Tasks;
using LoanLeadMs.Api.Models;
using LoanLeadMs.Api.Repositories;
using LoanLeadMs.Api.Services;
using NSubstitute;
using Xunit;

namespace LoanLeadMs.UnitTests
{
    public class CommunicationServiceTests
    {
        [Fact]
        public async Task AddCommunicationReturnsNullIfLeadNotPresent()
        {
            //Arrange 
            var mockLeadRepo = Substitute.For<ILoanLeadRepository>();
            mockLeadRepo.Get(Arg.Any<int>()).Returns((LoanLead)null);
            var service = new CommunicationService(null, mockLeadRepo);
            var logToAdd = GetCommunicationLogForAdding();
            //Act
            var result = await service.Add(logToAdd);

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task AddCommunicationReturnsSucceedsWhenLeadIsPresent()
        {
            //Arrange 
            var mockLeadRepo = Substitute.For<ILoanLeadRepository>();
            mockLeadRepo.Get(Arg.Any<int>()).Returns(Task.FromResult(new LoanLead { Id = 1001 }));
            mockLeadRepo.UpdateStatus(Arg.Any<int>(), Arg.Any<LeadStatus>()).Returns(Task.FromResult(true));
            var mockCommunicationRepo = Substitute.For<ICommunicationRepository>();
            mockCommunicationRepo.Add(Arg.Any<CommunicationLog>()).Returns(Task.FromResult(GetCommunicationLogForRepo()));
            var service = new CommunicationService(mockCommunicationRepo, mockLeadRepo);
            var logToAdd = GetCommunicationLogForAdding();
            //Act
            var result = await service.Add(logToAdd);

            //Assert
            Assert.NotNull(result);
            Assert.Equal(101, result.Id);
        }

        private CommunicationLog GetCommunicationLogForAdding() => new CommunicationLog
        {
            CommunicationMode = CommunicationMode.Call,
            LeadId = 1001,
            LeadStatus = LeadStatus.FollowUp
        };

        private CommunicationLog GetCommunicationLogForRepo() => new CommunicationLog
        {
            CommunicationMode = CommunicationMode.Call,
            LeadId = 1001,
            LeadStatus = LeadStatus.FollowUp,
            Id = 101
        };
    }
}
