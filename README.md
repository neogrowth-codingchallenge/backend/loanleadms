# LoanLeadMS

API for adding and getting Loan leads and Communication Logs
Configured to run in port 5503.
Base address is http://localhost:5503/api

- .NET Core APIs containerised using Docker
- PostgreSql used as Database
- Dapper as ORM
- To make api and db in same network, used Docker-compose
- Added swagger documentation
- Unit tests added for a service - using xUnit and NSubstitute
- Database migrations are handled using "Evolve"
- Logging is by Serilog

## Commands
* To build - docker-compose build
* To Run - docker-compose up

## Swagger Documentation

http://localhost:5503/swagger

## Some of the items in the wishlist (I wanted to implement, but couldn't)

- moving types to db
- caching for types
- Authentication and Authorisation
- Exception handler middleware
- Unification of responses

