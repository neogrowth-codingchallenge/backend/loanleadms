FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
LABEL servicename="loanleadms"
LABEL buildstage="loanleadms-build"
WORKDIR /app

# Copy files
COPY *.sln .
COPY ./src ./src
COPY ./tests ./tests
RUN dotnet restore

# Test
FROM build AS test
LABEL servicename=loanleadms
LABEL buildstage=loanleadms-test
RUN dotnet test -c Release --results-directory /testresults/ --logger "trx;LogFileName=testresults.trx" 

# Build
COPY . ./
FROM build AS publish
ARG APP_VERSION=0.0.0
ARG BUILD_NUMBER=0
ARG COMMIT_HASH=""
LABEL servicename=loanleadms
LABEL buildstage=loanleadms-publish
RUN dotnet publish ""src/LoanLeadMs.Api/LoanLeadMs.Api.csproj"" -c Release -o out

# Build image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
LABEL servicename=loanleadms
LABEL buildstage=loanleadms-release
WORKDIR /app
COPY --from=publish /app/out .
EXPOSE 80
ENTRYPOINT ["dotnet", "LoanLeadMs.Api.dll"]